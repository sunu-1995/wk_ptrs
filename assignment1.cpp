#include<iostream>
#include<memory>
using namespace std;
class sptr
{
    int* ptr;
    public:
     sptr(int* p= NULL)
    {
        ptr=p;
     cout<<"created  "<<*p<<endl;
    }
    ~sptr()
    {
        delete(ptr);
        cout<<"deleted"<<endl;
    }
    int& operator*()
    {
        return *ptr;
    }
};
int main()
{
      weak_ptr<sptr> w0;
        {
          shared_ptr<sptr>sharedsptr=make_shared<sptr>();
          weak_ptr<sptr>weaksptr=sharedsptr;
           w0=sharedsptr;
        }
      sptr ptr(new int());
      return *ptr;
}